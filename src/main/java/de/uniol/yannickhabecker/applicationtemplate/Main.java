package de.uniol.yannickhabecker.applicationtemplate;

import de.uniol.yannickhabecker.applicationtemplate.core.application.controller.StageController;
import de.uniol.yannickhabecker.applicationtemplate.core.applicationdetector.ApplicationDetectorController;
import de.uniol.yannickhabecker.applicationtemplate.core.applicationdetector.model.exception.ApplicationAlreadyRunningException;
import de.uniol.yannickhabecker.applicationtemplate.core.internationalization.Internationalization;
import de.uniol.yannickhabecker.applicationtemplate.core.properties.Configuration;
import de.uniol.yannickhabecker.applicationtemplate.shared.controller.AlertsController;
import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.util.Locale;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
        ApplicationDetectorController.setStopped();
    }

    @Override
    public void start(Stage primaryStage) {
        System.setProperty("file.encoding", "UTF-8");
        Internationalization.setLocale(Locale.forLanguageTag(Configuration.get("DEFAULT_LANGUAGE")));
        try {
            //Sage dem ApplicationDetectorController, dass das Programm jetzt gestartet wird
            //dieser wirft eine Fehlermeldung wenn eine Instanz bereits im gleichen Verzeichnis läuft
            //und damit Fehler mit der derby datenbank geschehen könnten.
            ApplicationDetectorController.setStarted();
            startApplication(primaryStage);
        } catch (ApplicationAlreadyRunningException e) {
            AlertsController.error("application.already.running");
        }
    }

    private void startApplication(Stage primaryStage) {
        //Zeige zunächst den Splashscreen und lade währenddessen den Default GlobstageController
        Label root = new Label(Internationalization.getString("splashscreen.title"));
        root.setAlignment(Pos.BOTTOM_LEFT);
        root.setStyle("-fx-background-color:transparent; -fx-background-image: url('/splashscreen.png'); -fx-background-size: stretch; -fx-text-fill: white; -fx-opacity: 0.5;");

        Scene scene = new Scene(root, 600, 300, Color.TRANSPARENT);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.show();

        //Initialisiere Default-Stage
        StageController stageController = new StageController("Neu");
        Stage stage = stageController.getStage();
        PauseTransition transition = new PauseTransition(Duration.seconds(Integer.parseInt(Configuration.get("SPLASH_SCREEN_TIMEOUT"))));
        transition.setOnFinished(event -> {
            //Zeige nun Default-Stage an und verstecke den Splashscreen
            stage.show();
            primaryStage.close();
        });
        transition.play();
    }


}

