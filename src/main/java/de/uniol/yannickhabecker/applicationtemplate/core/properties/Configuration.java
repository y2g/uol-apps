package de.uniol.yannickhabecker.applicationtemplate.core.properties;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

//Hilfsklasse für Konfigurationsdateien
public class Configuration {
    private  static Properties properties;
    public static String get(String key){
        if(properties == null)
            loadProperties();
        return properties.getProperty(key);
    }

    private static void loadProperties() {
        properties = new Properties();
        try {
            properties.load(Configuration.class.getResourceAsStream("/default.properties"));
            properties.load(new FileInputStream("configuration.properties"));

        } catch (IOException ignored) {

        }
    }

    public static int getInt(String key) {
        return Integer.parseInt(get(key));
    }

    public static boolean getBoolean(String key) {
        return Boolean.parseBoolean(get(key));
    }
}
