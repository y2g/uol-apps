package de.uniol.yannickhabecker.applicationtemplate.core.file;

import de.uniol.yannickhabecker.applicationtemplate.core.internationalization.Internationalization;
import de.uniol.yannickhabecker.applicationtemplate.shared.view.MenuView;
import javafx.scene.control.Menu;

public class FileMenuView extends MenuView<FileController> {
    private Menu menu = new Menu();

    public FileMenuView(FileController controller) {
        super(controller);
        init();
    }

    @Override
    protected void bind() {

    }

    @Override
    protected void layout() {
    }

    @Override
    public Menu getMenu() {
        return menu;
    }

    @Override
    public void updateText() {
        menu.setText(Internationalization.getString("menu.file.title"));
    }
}
