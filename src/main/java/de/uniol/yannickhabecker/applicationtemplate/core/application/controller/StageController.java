package de.uniol.yannickhabecker.applicationtemplate.core.application.controller;

import de.uniol.yannickhabecker.applicationtemplate.core.application.view.TaskBarView;
import de.uniol.yannickhabecker.applicationtemplate.core.application.view.ToolBarView;
import de.uniol.yannickhabecker.applicationtemplate.core.file.FileController;
import de.uniol.yannickhabecker.applicationtemplate.core.file.FileMenuView;
import de.uniol.yannickhabecker.applicationtemplate.core.internationalization.Internationalization;
import de.uniol.yannickhabecker.applicationtemplate.core.internationalization.LanguageController;
import de.uniol.yannickhabecker.applicationtemplate.core.internationalization.LanguageMenuView;
import de.uniol.yannickhabecker.applicationtemplate.shared.controller.Controller;
import de.uniol.yannickhabecker.applicationtemplate.shared.events.SetLanguageEvent;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.Locale;

//Dieser Controller ermöglicht das erstellen von Stages

public class StageController {
    private final Stage stage = new Stage();
    private final BorderPane mainWindowPane;
    private final BorderPane innerBorderPane;
    private final Scene scene;
    private final MenuBar mainMenu;
    private final SplitPane workspacePane;
    private final ToolBarView toolBarView;
    private final ToolBarController toolBarController;
    private final TaskBarController taskBarController;
    private final TaskBarView taskBarView;
    private final FileMenuView fileMenuView;
    private final LanguageMenuView languageMenuView;
    private final FileController fileController;
    private String name;

    public Stage getStage() {
        return stage;
    }

    //Erstellt eine neue Stage mit einem Programm und einem Namen
    public StageController(String name) {
        this.name = name;

        this.toolBarController = new ToolBarController();
        this.taskBarController = new TaskBarController();

        //Views initialisieren

        //Titel der Stage ändern
        stage.setTitle(Internationalization.getString("title", this.name));

        //BorderPanes für den Aufbau des Fensters erstellen
        mainWindowPane = new BorderPane();
        innerBorderPane = new BorderPane();
        mainWindowPane.setCenter(innerBorderPane);

        //Scene mit dem Eltern-Pane erstellen
        scene = new Scene(mainWindowPane, 800, 600, Color.WHITE);

        //Hauptmenü initialisieren
        mainMenu = new MenuBar();

        //Gefunden auf: http://stackoverflow.com/questions/22569046/how-to-make-an-os-x-menubar-in-javafx
        final String os = System.getProperty ("os.name");
        if (os != null && os.startsWith ("Mac"))
            mainMenu.useSystemMenuBarProperty ().set (true);

        fileController = new FileController();
        fileMenuView = new FileMenuView(fileController);
        languageMenuView = new LanguageMenuView(new LanguageController());
        //Alle Menüs zum Hauptmenü hinzufügen
        mainMenu.getMenus().addAll(fileMenuView.getMenu(), languageMenuView.getMenu());


        //Toolbar für schnellen Aufruf von Funktionen initialisieren
        toolBarView = new ToolBarView(toolBarController);

        //Taskbar erstellen
        taskBarView = new TaskBarView(taskBarController);

        //Splitpane mit Editor und Territorium initalisieren
        workspacePane = new SplitPane();
        //workspacePane.getItems().addAll(editorView.getParent(), territoryView.getParent());
        workspacePane.setOrientation(Orientation.HORIZONTAL);

        //Views in Fenster anordnen
        mainWindowPane.setTop(mainMenu);
        innerBorderPane.setTop(toolBarView.getParent());
        innerBorderPane.setCenter(workspacePane);
        mainWindowPane.setBottom(taskBarView.getParent());

        stage.setScene(scene);


        //Bindings erstellen
        //stage.setOnShown(event -> territoryView.render());


        //Wenn der Benutzer die Sprache ändert müssen die View benachrichtigt werden um die Texte anzupassen
        ChangeListener<Locale> changeListener = (observable, oldValue, newValue) -> {
            Controller.CONTROLLER_BUS.post(new SetLanguageEvent());

            //Alle Menüs zum Hauptmenü hinzufügen (Mac OS X Workaround (Texte aktualisieren))
            mainMenu.getMenus().clear();
            mainMenu.getMenus().addAll(fileMenuView.getMenu(), languageMenuView.getMenu());


            stage.setTitle(Internationalization.getString("title", this.name));
        };

        Internationalization.addLocaleListener(changeListener);

        //Wenn das Fenster geschlossen wird muss der Listener wieder entfernt werden
        stage.setOnCloseRequest(event -> {
            Internationalization.removeLocaleListener(changeListener);
            //TODO editorController.save();
            // editorController.kill();
        });

        Controller.CONTROLLER_BUS.post(new SetLanguageEvent());
    }
}
