package de.uniol.yannickhabecker.applicationtemplate.shared.view;

import de.uniol.yannickhabecker.applicationtemplate.shared.controller.Controller;
import javafx.scene.Parent;


//Die ParentView Klasse ist die Oberklasse aller Views, die ein Parent-Objekt erstellen und zurückgeben sollen.

public abstract class ParentView<C extends Controller> extends View<C> {
    protected ParentView(C controller) {
        super(controller);
    }

    //Gibt von der View-Klasse verwalteten JavaFX-View zurück
    public abstract Parent getParent();
}
