package de.uniol.yannickhabecker.applicationtemplate.core.applicationdetector.model.exception;

//Exception, die geworfen wird, wenn die Anwendung bereits läuft.
public class ApplicationAlreadyRunningException extends Throwable {
}
