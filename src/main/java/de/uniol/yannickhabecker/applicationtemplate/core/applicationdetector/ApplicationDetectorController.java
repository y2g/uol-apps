package de.uniol.yannickhabecker.applicationtemplate.core.applicationdetector;

import de.uniol.yannickhabecker.applicationtemplate.core.applicationdetector.model.PortContainer;
import de.uniol.yannickhabecker.applicationtemplate.core.applicationdetector.model.exception.ApplicationAlreadyRunningException;
import de.uniol.yannickhabecker.applicationtemplate.shared.controller.AlertsController;
import de.uniol.yannickhabecker.applicationtemplate.shared.controller.Controller;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ApplicationDetectorController extends Controller {
    private static int PORT;		// random large port number
    private final static String PORT_FILE = "port.lock";
    private static ServerSocket socket;

    // Ermittelt einen freien Port und speichert ihn in der Datei port.lock, falls nicht bereits eine ports.lock datei existiert
    // und damit, falls der dortige Port belegt ist, bereits eine Anwendung läuft.
    public static void setStarted() throws ApplicationAlreadyRunningException {
        if(PORT==0){
            Integer filePort = getPortFromFile();
            if(filePort != null) {
                try {
                    socket = new ServerSocket(filePort, 10, InetAddress.getLocalHost());
                } catch (IOException e) {
                    throw new ApplicationAlreadyRunningException();
                }
            }

            PORT = createPort();
            try {
                socket = new ServerSocket(PORT, 10, InetAddress.getLocalHost());
                //Benutzte Ports speichern:
                //Dafür wird PortContainer verwendet, der speichert einen einzigen Port und kann serialisiert werden
                try (
                        OutputStream outputStream = new FileOutputStream(PORT_FILE);
                        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)
                ){
                    objectOutputStream.writeObject(new PortContainer(PORT));
                } catch (IOException ignored) {
                    ignored.printStackTrace();
                    AlertsController.error("alert.unknownerror");
                }
            } catch (IOException ignored) {
            }

        }
    }

    //Löscht die ports datei, so dass alle Ports wieder freigegeben wurdne.
    public static void setStopped(){
        Path path = Paths.get(PORT_FILE);
        if(Files.exists(path)){
            try {
                Files.delete(path);
            } catch (IOException ignored) {
            }
        }
    }

    //Liest den Port aus der ports.lock Datei
    private static Integer getPortFromFile(){
        Path path = Paths.get(PORT_FILE);
        if (Files.exists(path)){
            try (
                    InputStream inputStream = new FileInputStream(PORT_FILE);
                    ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)
            ) {
                Object object = objectInputStream.readObject();
                if (object instanceof PortContainer) {
                    PortContainer portContainer = (PortContainer) object;
                    return portContainer.getPort();
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    //Findet freien Port und belegt ihn
    private static int createPort() {
        boolean portBound = true;
        int newPort = 0;
        while(portBound){
            newPort = (int)(47000*Math.random()+2000.0);
            try{
                socket = new ServerSocket(PORT, 10, InetAddress.getLocalHost());
                portBound = false;
            } catch (IOException e) {
                //Port wird bereits verwendet
            }
        }
        return newPort;
    }


}
