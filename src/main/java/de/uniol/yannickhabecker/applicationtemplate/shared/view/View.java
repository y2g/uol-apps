package de.uniol.yannickhabecker.applicationtemplate.shared.view;


import com.google.common.eventbus.Subscribe;
import de.uniol.yannickhabecker.applicationtemplate.shared.controller.Controller;
import de.uniol.yannickhabecker.applicationtemplate.shared.events.SetLanguageEvent;

//Oberklasse aller Views, generalisiert mit einem beliebigen zum View passenden Controller
//e.g. LanguageMenuView<LanguageController>
public abstract class View<C extends Controller> {
    protected C controller;

    protected View(C controller) {
        this.controller = controller;
        Controller.CONTROLLER_BUS.register(this);
    }

    protected void init() {
        bind();
        layout();
    }

    protected abstract void layout();

    protected abstract void bind();

    /**
     * Methode, die die in der View gegebenen Texte mithilfe der Internationalisierung übersetzt
     */
    public abstract void updateText();

    @Subscribe
    public void setLanguageEvent(SetLanguageEvent event) {
        updateText();
    }
}
