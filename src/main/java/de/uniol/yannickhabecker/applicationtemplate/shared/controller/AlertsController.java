package de.uniol.yannickhabecker.applicationtemplate.shared.controller;

import de.uniol.yannickhabecker.applicationtemplate.core.internationalization.Internationalization;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class AlertsController extends Controller{
    public static void error(String identifier){
        Platform.runLater(()->{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(Internationalization.getString("alert.error"));
            alert.setHeaderText(Internationalization.getString(identifier+".header"));
            alert.setContentText(Internationalization.getString(identifier+".text"));
            alert.show();
        });
    }

    public static void textAreaAlert(String identifier, String alertText) {
        Platform.runLater(()-> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(Internationalization.getString("alert.error"));
            alert.setHeaderText(Internationalization.getString(identifier + ".header"));
            alert.setContentText(Internationalization.getString(identifier + ".text"));

            TextArea textArea = new TextArea(alertText);
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(textArea, 0, 1);

            alert.getDialogPane().setExpandableContent(expContent);

            alert.show();
        });

    }

    public static void success(String identifier) {
        Platform.runLater(()-> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(Internationalization.getString("alert.information"));
            alert.setHeaderText(Internationalization.getString("alert.information"));
            alert.setContentText(Internationalization.getString(identifier + ".text"));
            alert.show();
        });
    }
}
