package de.uniol.yannickhabecker.applicationtemplate.shared.controller;

import com.google.common.eventbus.EventBus;

import java.util.Observable;

//Oberklasse aller Controller
public class Controller extends Observable {
    public final static EventBus CONTROLLER_BUS = new EventBus();

    public Controller() {
        CONTROLLER_BUS.register(this);
    }

    protected static EventBus bus() {
        return CONTROLLER_BUS;
    }
}
