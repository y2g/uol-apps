package de.uniol.yannickhabecker.applicationtemplate.core.application.view;


import de.uniol.yannickhabecker.applicationtemplate.core.internationalization.Internationalization;
import de.uniol.yannickhabecker.applicationtemplate.shared.controller.Controller;
import de.uniol.yannickhabecker.applicationtemplate.shared.view.ParentView;
import javafx.scene.Parent;
import javafx.scene.control.Label;

//Taskbar-View wird am unteren Rand des Stages angezeigt
public class TaskBarView extends ParentView<Controller> {
    Label taskBar = new Label();

    public TaskBarView(Controller controller) {
        super(controller);
        init();
    }

    @Override
    protected void layout() {

    }

    @Override
    protected void bind() {

    }

    @Override
    public Parent getParent() {
        return taskBar;
    }


    @Override
    public void updateText() {
        taskBar.setText(Internationalization.getString("welcome"));
    }
}
