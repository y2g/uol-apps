package de.uniol.yannickhabecker.applicationtemplate.shared.model;

import com.google.common.eventbus.EventBus;

import java.util.Observable;

//Oberklasse aller Models
public class Model extends Observable {
    private final static EventBus MODEL_BUS = new EventBus();

    public Model() {
        MODEL_BUS.register(this);
    }

    protected static EventBus bus() {
        return MODEL_BUS;
    }
}
