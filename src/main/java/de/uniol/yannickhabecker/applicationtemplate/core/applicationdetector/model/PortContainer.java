package de.uniol.yannickhabecker.applicationtemplate.core.applicationdetector.model;

import java.io.Serializable;

//Der Port-Container speichert einen einzelnen port und kann serialisiert werden.
public class PortContainer implements Serializable {
    private Integer port;

    public PortContainer(Integer port) {
        this.port = port;
    }

    public Integer getPort() {
        return port;
    }
}
