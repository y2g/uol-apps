package de.uniol.yannickhabecker.applicationtemplate.core.internationalization;

import de.uniol.yannickhabecker.applicationtemplate.shared.view.MenuView;
import javafx.scene.control.Menu;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ToggleGroup;

import java.util.Locale;

//Generiert das Sprachmenü
public class LanguageMenuView extends MenuView<LanguageController> {
    private Menu languageMenu = new Menu();
    private ToggleGroup languageToggleGroup = new ToggleGroup();
    private RadioMenuItem german = new RadioMenuItem();
    private RadioMenuItem english = new RadioMenuItem();

    public LanguageMenuView(LanguageController controller) {
        super(controller);
        init();
    }

    @Override
    public Menu getMenu() {
        return languageMenu;
    }

    //Layout erzeugen
    @Override
    protected void layout(){
        languageMenu.getItems().addAll(german, english);

        german.setToggleGroup(languageToggleGroup);
        german.setSelected(true);
        english.setToggleGroup(languageToggleGroup);
        languageMenu.setMnemonicParsing(true);
        //Locales auf sprachen setzen
        german.setOnAction(event -> controller.setLocale(Locale.GERMAN));
        english.setOnAction(event -> controller.setLocale(Locale.ENGLISH));
    }

    @Override
    protected void bind() {

    }

    @Override
    public void updateText() {
        languageMenu.setText(Internationalization.getString("languages"));
        german.setText(Internationalization.getString("german"));
        english.setText(Internationalization.getString("english"));

        //Häckchen im Sprachmenü setzen
        if(Internationalization.getLocale().getLanguage().equals("en")){
            english.setSelected(true);
        }else{
            german.setSelected(true);
        }
    }
}
