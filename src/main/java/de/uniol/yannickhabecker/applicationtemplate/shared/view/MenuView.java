package de.uniol.yannickhabecker.applicationtemplate.shared.view;

import de.uniol.yannickhabecker.applicationtemplate.shared.controller.Controller;
import javafx.scene.control.Menu;


//Die MenuView Klasse ist die Oberklasse aller Menu-Views
public abstract class MenuView<C extends Controller> extends View<C> {
    protected MenuView(C controller) {
        super(controller);
    }


    //Gibt Menü des Views zurück
    public abstract Menu getMenu();
}
