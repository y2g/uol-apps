package de.uniol.yannickhabecker.applicationtemplate.core.application.view;

import de.uniol.yannickhabecker.applicationtemplate.core.application.controller.ToolBarController;
import de.uniol.yannickhabecker.applicationtemplate.shared.view.ParentView;
import javafx.scene.Parent;
import javafx.scene.control.ToolBar;


//Die Toolbar wird im Stage-Fenster oben angezeigt und enthält Schaltflächen mit den wichtigsten Aktionen
public class ToolBarView extends ParentView<ToolBarController> {
    private ToolBar view = new ToolBar();

    //Schaltflächen der Toolbar initialisieren

    public ToolBarView(ToolBarController controller) {
        super(controller);
        init();
    }
    //Aktionen an controller weiterleiten
    @Override
    protected void bind() {
        //events der buttons zum controller leiten.

    }

    @Override
    protected void layout(){
        //view.getItems().addAll(buttonA, buttonB);

    }

    @Override
    public Parent getParent() {
        return view;
    }

    @Override
    public void updateText() {
        //newButton.setTooltip(new Tooltip(Internationalization.getString("new")));
    }


}
