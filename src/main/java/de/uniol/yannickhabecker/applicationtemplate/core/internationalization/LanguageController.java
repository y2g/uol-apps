package de.uniol.yannickhabecker.applicationtemplate.core.internationalization;


import de.uniol.yannickhabecker.applicationtemplate.shared.controller.Controller;

import java.util.Locale;

//Controller für das Sprach-Menü
public class LanguageController extends Controller {
    public void setLocale(Locale locale) {
        Internationalization.setLocale(locale);
    }


}
