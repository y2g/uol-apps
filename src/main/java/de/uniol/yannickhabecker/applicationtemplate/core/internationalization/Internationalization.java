package de.uniol.yannickhabecker.applicationtemplate.core.internationalization;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

//Hilfsklasse für die Unterstützung verschiedener Sprachen
public class Internationalization {
    private static ResourceBundle bundle;
    private static ObjectProperty<Locale> localeObservable = new SimpleObjectProperty<>();


    //Gibt Text aus der Sprachdatei zurück
    public static String getString(String name){
        if(bundle == null)
            load();
        String val = bundle.getString(name);
        try {
            return new String(val.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Gibt text aus der Sprachdatei zurück und ersetzt Vorkommen von %s durch Ersetzungen
    public static String getString(String name, String... replacements){
        String text = getString(name);
        for(String replace:replacements){
            text = text.replaceFirst("%s", Matcher.quoteReplacement(replace));
        }

        return text;
    }

    public static void load(){
        bundle = ResourceBundle.getBundle("internationalization");
    }

    //Setzt die Locale
    public static void setLocale(Locale locale){
        Locale.setDefault(locale);
        load();
        localeObservable.set(locale);
    }

    //Listener, die aufgerufen werden wenn die Sprache geändert wird.
    public static void addLocaleListener(ChangeListener<Locale> changeListener){
        localeObservable.addListener(changeListener);
    }

    public static void removeLocaleListener(ChangeListener<Locale> changeListener){
        localeObservable.removeListener(changeListener);
    }

    public static Locale getLocale(){
        return localeObservable.get();
    }

}
